name := "scalaz-practice"
version := "0.0.1"
organization := "com.technallurgy"

scalaVersion := "2.11.4"

val scalazVersion = "7.1.3"
libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-reflect" % "2.11.7",
  "org.scala-lang" % "scala-compiler" % "2.11.7",
  "org.scalaz" %% "scalaz-core" % scalazVersion,
  "org.scalaz" %% "scalaz-effect" % scalazVersion,
  "org.scalaz" %% "scalaz-typelevel" % scalazVersion,
  "org.scalaz" %% "scalaz-scalacheck-binding" % scalazVersion % "test",
  "org.apache.avro" % "avro" % "1.7.7"

)

scalacOptions += "-feature"
initialCommands in console := "import scalaz._, Scalaz._"
