learning-scalaz
===============

Learning Scalaz
Should be able to chain several GENERIC functions together, such as Flatten -> Distinct -> Max,
which could in theory operate over a wide domain of input (ints, longs, floats, maybe even bools or strings).

Scalaz is, from what I can see, a library that implements Haskell-like functional programming things, like
Monoids, Monads, Applicative Functors, etc. This repo is my scratchpad for working through the examples in
[Learning Scalaz (in 21 days)](http://eed3si9n.com/learning-scalaz/).

It also references Learn You a Haskell for Great Good, which provides essential background knowledge on
Functional Programming aspects used throughout the Scalaz tutorial.
