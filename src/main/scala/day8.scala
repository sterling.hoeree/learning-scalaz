package scalaz.practice

object Day8 extends App {
    import scalaz._
    import Scalaz._

    def expect(name: String, expected: Any, actual: Any) = {
        println( "Test [" + name + "] is " + actual.toString )
        assert(expected == actual, "FAILED: " + expected + " != to resulting " + actual)
    }

    // Note: Pariya and I found a way without Scalaz!
    //
    //   def foo[A <% Ordered[A]](xs: List[A]) = xs.max
    //
    // Chaining a bunch of stuff:
    //
    //   def bar[A](xs: List[A]) = xs.distinct
    //   def wrap[A](x: A) = List(x)
    //   val f = (foo[Int] _ andThen wrap _ andThen bar)
    //   f(List(1,2,82,2,12894,128)) // --> List[Int] = List(12894)
    //

    // Easy functions.
    def Iden[A] = Kleisli { (x: A) => x.some }
    def Length = Kleisli { (x: String) => x.length.some }
    def Count[A] = Kleisli { x: List[A] => x.size.some }
    def Repeat[A](n: Int) = Kleisli { x: A => {
        val c = collection.mutable.ListBuffer[A]()
        for (_ <- 1 to n) { c += x }
        c.toList.some
    }}
    def Distinct[A] = Kleisli { x: List[A] => x.toSet.toList.some }

    // Harder functions.
    // NOTE: for implicits to work properly, make sure that (s|S)calaz._ is
    // imported in *this* context (i.e. this object)! Globally won't work!
    def Max[A: Order] = Kleisli { x: List[A] => Foldable[List].maximum(x) }
    def Min[A: Order] = Kleisli { x: List[A] => Foldable[List].minimum(x) }
    def Sum[A: Monoid] = Kleisli { x: List[A] => x.suml.some }

    override def main(args: Array[String]) = {
        expect("id-repeat-count", 10.some,
            10.some >>= (Iden >=> Repeat[Int](10) >=> Count))
        expect("id-repeat-distinct-count", 1.some,
            10.some >>= (Iden >=> Repeat[Int](10) >=> Distinct >=> Count))
        expect("id-repeat-max", 30.some,
            30.some >>= (Iden >=> Repeat[Int](3) >=> Max))
        // XXX: the below won't work.
        // val ops = List(Iden, Repeat[Int](10), Distinct, Iden)
        // expect("list/id-repeat-distinct-id", 10.some,
        //     10.some >>= ops.reduceLeft(_ >=> _))
    }
}
