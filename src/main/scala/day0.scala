package scalaz.practice

trait Day0Monoid[A] {
    def +(a1: A, a2: A): A

    def > (a1: A, a2: A): Boolean
    def < (a1: A, a2: A): Boolean
    def ==(a1: A, a2: A): Boolean

    def z: A // Zero
}

object Day0Monoid {
    implicit val IntMonoid = new Day0Monoid[Int] {
        def + (a1: Int, a2: Int) = a1 + a2

        def > (a1: Int, a2: Int) = a1 > a2
        def < (a1: Int, a2: Int) = a1 < a2
        def ==(a1: Int, a2: Int) = a1 == a2

        def z = 0
    }

    implicit val LongMonoid = new Day0Monoid[Long] {
        def + (a1: Long, a2: Long) = a1 + a2

        def > (a1: Long, a2: Long) = a1 > a2
        def < (a1: Long, a2: Long) = a1 < a2
        def ==(a1: Long, a2: Long) = a1 == a2

        def z = 0L
    }

    implicit val StringMonoid = new Day0Monoid[String] {
        def + (a1: String, a2: String) = a1 + a2

        def > (a1: String, a2: String) = (a1 compareToIgnoreCase a2) < 0
        def < (a1: String, a2: String) = (a1 compareToIgnoreCase a2) > 0
        def ==(a1: String, a2: String) = a1 equalsIgnoreCase a2

        def z = ""
    }

    implicit val FloatMonoid = new Day0Monoid[Float] {
        def + (a1: Float, a2: Float) = a1 + a2

        def > (a1: Float, a2: Float) = a1 > a2
        def < (a1: Float, a2: Float) = a1 < a2
        def ==(a1: Float, a2: Float) = a1 == a2

        def z = 0.0f
    }

    implicit val DoubleMonoid = new Day0Monoid[Double] {
        def + (a1: Double, a2: Double) = a1 + a2

        def > (a1: Double, a2: Double) = a1 > a2
        def < (a1: Double, a2: Double) = a1 < a2
        def ==(a1: Double, a2: Double) = a1 == a2

        def z = 0.0
    }

    implicit val BooleanMonoid = new Day0Monoid[Boolean] {
        def + (a1: Boolean, a2: Boolean) = a1 || a2

        def > (a1: Boolean, a2: Boolean) =  a1 && !a2
        def < (a1: Boolean, a2: Boolean) = !a1 &&  a2
        def ==(a1: Boolean, a2: Boolean) =  a1 ==  a2

        def z = false
    }
}

trait FoldLeft[I[_]] {
    def head[X](xs: I[X]): X
    def foldLeft[X, Y](xs: I[X], init: Y, func: (Y, X) => Y): Y
    def foldLeftZ[X](xs: I[X], func: (X, X) => X): X
}

object FoldLeft {
    implicit val FoldLeftIterable: FoldLeft[Iterable] = new FoldLeft[Iterable] {
        def head[X](xs: Iterable[X]): X = xs.head
        def foldLeft[X, Y](xs: Iterable[X], init: Y, func: (Y, X) => Y): Y = xs.foldLeft(init)(func)
        def foldLeftZ[X](xs: Iterable[X], func: (X, X) => X): X =
            if (xs.tail.nonEmpty) xs.tail.foldLeft(xs.head)(func)
            else xs.head
    }

    implicit val FoldLeftSeq: FoldLeft[Seq] = new FoldLeft[Seq] {
        def head[X](xs: Seq[X]): X = xs.head
        def foldLeft[X, Y](xs: Seq[X], init: Y, func: (Y, X) => Y): Y = xs.foldLeft(init)(func)
        def foldLeftZ[X](xs: Seq[X], func: (X, X) => X): X =
            if (xs.tail.nonEmpty) xs.tail.foldLeft(xs.head)(func)
            else xs.head
    }

    implicit val FoldLeftList: FoldLeft[List] = new FoldLeft[List] {
        def head[X](xs: List[X]): X = xs.head
        def foldLeft[X, Y](xs: List[X], init: Y, func: (Y, X) => Y): Y = xs.foldLeft(init)(func)
        def foldLeftZ[X](xs: List[X], func: (X, X) => X): X =
            if (xs.tail.nonEmpty) xs.tail.foldLeft(xs.head)(func)
            else xs.head
    }
}

trait MonoidOp[A] {
    val m: Day0Monoid[A]
    val a1: A
    def |+|(a2: A) = m.+(a1, a2)
}

object MonoidOps {
    implicit def toMonoidOp[A: Day0Monoid](a: A): MonoidOp[A] = new MonoidOp[A] {
        val m = implicitly[Day0Monoid[A]]
        val a1 = a
    }
}

object Day0Builtins {
    import MonoidOps._

    def sum[M[_]: FoldLeft, A: Day0Monoid](xs: M[A]): A = {
        val m = implicitly[Day0Monoid[A]]
        val fl = implicitly[FoldLeft[M]]
        fl.foldLeft(xs, m.z, (acc: A, a: A) => acc |+| a)
    }

    def max[M[_]: FoldLeft, A: Day0Monoid](xs: M[A]): A = {
        val m = implicitly[Day0Monoid[A]]
        val fl = implicitly[FoldLeft[M]]
        fl.foldLeftZ(xs, (acc: A, x: A) => if (m.>(acc,x)) acc else x)
    }

    def `if`[A: Day0Monoid](p: Boolean, xs: Iterable[A]): Iterable[A] = {
        implicitly[Day0Monoid[A]]
        if (p) xs else Seq[A]()
    }
}

object Day0 extends App {
    import Day0Monoid._
    import FoldLeft._
    import MonoidOps._

    override def main(args: Array[String]) = {
        println("Running tests...")

        val intSeq = Seq(1,2,3,4,5)
        assert(15 == Day0Builtins.sum(intSeq), "failed sum[int]")

        val longSeq = Seq(1L,2L,3L,4L,5L)
        assert(15 == Day0Builtins.sum(longSeq), "failed sum[long]")

        val strSeq = Seq("One ", "Two ", "Three ", "Four ", "Five")
        assert("One Two Three Four Five" equals Day0Builtins.sum(strSeq), "failed sum[string]")

        val dblSeq = Seq(1.1,2.2,3.3,4.4,5.5)
        assert(16.5 == Day0Builtins.sum(dblSeq), "failed sum[double]")

        val fltSeq = Seq(1.1f,2.2f,3.3f,4.4f,5.5f)
        assert(16.5 == Day0Builtins.sum(fltSeq), "failed sum[float]")

        val boolSeq = Seq(false,false,false,false,false)
        assert(false == Day0Builtins.sum(boolSeq), "failed sum[FFFFF]")

        val boolSeq2 = Seq(false,false,true,false,false)
        assert(true == Day0Builtins.sum(boolSeq2), "failed sum[FFTFF]")

        assert((Day0Builtins.`if`(false, intSeq)).isEmpty, "failed if[false, int]")
        assert((Day0Builtins.`if`(true, intSeq)).nonEmpty, "failed if[true, int]")

        val findMaxSeq = Seq(1,2,3,4,5,6,5,4,3,2,1)
        assert(6 == Day0Builtins.max(findMaxSeq), "failed max[int]")

        val findMaxSeq2 = Seq(50.0,51.2,54.5,99.9,-1.111,98.6753,100.0, -100.0001)
        assert(100.0 == Day0Builtins.max(findMaxSeq2), "failed max[double]")

        assert(15 == (10 |+| 5), "failed MonoidOps.|+|[int]")
        assert("One Two" == ("One " |+| "Two"), "failed MonoidOps.|+|[string]")
        assert(true == (false |+| true), "failed MonoidOps.|+|[FT]")

        println("Done running tests.")
    }
}
