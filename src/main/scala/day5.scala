package scalaz.practice
import scalaz._
import Scalaz._

class Day5Pipe[A](val x: A) {
    override def toString = s"Pipe[$x]"
    override def equals(o: Any) = o match {
        case that: Day5Pipe[A] => that.x == this.x
        case _ => false
    }
}

object Day5Pipe {
    def apply[A](z: A): Option[Day5Pipe[A]] = (new Day5Pipe(z)).some
}

object Day5 extends App {
    def expect(name: String, expected: Any, actual: Any) = {
        println( "Test [" + name + "] is " + actual.toString )
        assert(expected == actual, "FAILED: " + expected + " != to resulting " + actual)
    }

    def putIf[A](p: Boolean, pi1: => Option[Day5Pipe[A]], pi2: => Option[Day5Pipe[A]]): Option[Day5Pipe[A]] =
        if (p) pi1 else pi2

    override def main(args: Array[String]) = {
        expect("some-pipe", Day5Pipe(1995),
            Day5Pipe(10) >>=
                {p => Day5Pipe(p.x + 10)} >>=
                    {p => putIf(p.x > 10, Day5Pipe(p.x * 100), p.some)} >>=
                        {p: Day5Pipe[Int] => Day5Pipe(p.x - 5)}
        )

        expect("none-pipe", none,
            (Day5Pipe(10) >>=
                {p => Day5Pipe(p.x + 10)} >>=
                    {p => putIf(p.x > 10, Day5Pipe(p.x * 100), p.some)}) >>
                        (none: Option[Day5Pipe[Int]]) >>=
                            {p => Day5Pipe(p.x - 5)}
        )

        expect("for/yield", Day5Pipe(1995), for {
            start  <- Day5Pipe(10)
            a      <- Day5Pipe(start.x + 10)
            b      <- putIf(a.x > 10, Day5Pipe(a.x * 100), a.some)
            c      <- Day5Pipe(b.x - 5)
        } yield c)

        val ops = List(
            (p: Day5Pipe[Int]) => Day5Pipe(p.x + 10),
            (p: Day5Pipe[Int]) => putIf(p.x > 10, Day5Pipe(p.x * 100), p.some),
            (p: Day5Pipe[Int]) => none: Option[Day5Pipe[Int]],
            (p: Day5Pipe[Int]) => Day5Pipe(p.x - 5)
            )
        expect("list-pipe", none, ops.foldLeft(Day5Pipe(10))((acc, next) => acc >>= next))

    }
}
