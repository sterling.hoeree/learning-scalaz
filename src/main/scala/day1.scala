package scalaz.practice
import scalaz._
import Scalaz._

// import scala.language.implicitConversions

trait Truthy[A] {
    def make(a: A): Boolean
}

object Truthy {
    def apply[A](implicit ev: Truthy[A]): Truthy[A] = ev
    def make[A](f: A => Boolean): Truthy[A] = new Truthy[A] {
        def make(a: A): Boolean = f(a) // Convert A -> Boolean
    }
}

trait TruthyOps[A] {
    val self: A
    def F: Truthy[A]
    def truthy: Boolean = F.make(self)
}

object ToTruthyOps {
    implicit def toTruthyOps[A: Truthy](v: A) = {
        new TruthyOps[A] {
            val self = v
            def F: Truthy[A] = implicitly[Truthy[A]]
        }
    }
}

object Day1 extends App {
    def expect(name: String, expected: Any, actual: Any) = {
        println( "Test [" + name + "] is " + name.toString )
        assert(expected == actual, "FAILED: " + expected + " != " + actual)
    }

    import ToTruthyOps._

    override def main(args: Array[String]) = {
        implicit val IntTruthy: Truthy[Int] = Truthy.make({
            case 0 => false
            case _ => true
        })

        import scala.math
        implicit val FloatTruthy: Truthy[Float] = Truthy.make({
            case v: Float if (math.abs(v) + 0.5f) >= 1.0f => true
            case _ => false
        })

        expect("1.truthy", true, 1.truthy)
        expect("0.truthy", false, 0.truthy)
        expect("(0.5f).truthy", true, (0.5f).truthy)
        expect("(0.49f).truthy", false, (0.49f).truthy)
    }
}
